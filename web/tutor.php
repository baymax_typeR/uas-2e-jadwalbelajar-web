<?php
    include 'koneksi_jb.php';
    include 'koneksi.php';
    $db = new database();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Tutor | Jadwal Belajar</title>
  </head>
  <body>
    <?php
        include 'navbar.php';
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="mt-3">Selamat Datang di Website Jadwal Belajar</h2>
                <h3 class="mt-1">Tutor</h3>
                <a href="input_tutor.php" class="btn btn-success my-3">Tambah Data Tutor</a>
                <br>                

                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">No.</th>                    
                        <th scope="col">ID Tutor</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Telepon</th>
                        <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($db->show_tutor() as $data) {
                        ?>
                        <tr class="table text-left">
                        <td><?php echo $no++; ?></td>
                        <td><?php                              
                                $char = "T";
                                $kode = $char . sprintf("%03s", $data['id_pengajar']);                       
                                echo $kode;
                            ?></td>
                        <td><?php echo $data['nama_pengajar']; ?></td>
                        <td><?php echo $data['alamat_pengajar']; ?></td>
                        <td><?php echo $data['telepon']; ?></td>                        
                        <td>                    
                            <a href="edit_tutor.php?id_pengajar=<?php echo $data['id_pengajar']; ?>&aksi_tutor=edit_tutor" class="btn btn-info">Edit</a>
                            <a href="proses_jb.php?id_pengajar=<?php echo $data['id_pengajar']; ?>&aksi_tutor=delete_tutor" class="btn btn-danger" onclick="return confirm('Yakin untuk menghapus data <?php echo $data['nama_pengajar'] ?>?')">Hapus</a>                        
                        </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>