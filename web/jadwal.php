<?php
    include 'koneksi_jb.php';
    include 'koneksi.php';
    $db = new database();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Jadwal | Jadwal Belajar</title>
  </head>
  <body>
    <?php
        include 'navbar.php';
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="mt-3">Selamat Datang di Website Jadwal Belajar</h2>
                <h3 class="mt-1">Jadwal</h3>
                <a href="input_jadwal.php" class="btn btn-success my-3">Tambah Data Jadwal</a>
                <br>                

                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">No.</th>                    
                        <th scope="col">ID Jadwal</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Nama Pengajar</th>
                        <th scope="col">Mata Pelajaran</th>
                        <th scope="col">Kelas</th>                        
                        <th scope="col">Jam</th>                        
                        <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($db->show_jadwal() as $data) {
                        ?>
                        <tr class="table text-left">
                        <td><?php echo $no++; ?></td>
                        <td><?php                              
                                $char = "J";
                                $kode = $char . sprintf("%03s", $data['id_jadwal']);                       
                                echo $kode;
                            ?></td>
                        <td><?php echo $data['tanggal']; ?></td>                        
                        <td><?php echo $data['nama_pengajar']; ?></td>                        
                        <td><?php echo $data['nama_mapel']; ?></td>                        
                        <td><?php echo $data['kelas']; ?></td>                        
                        <td><?php echo $data['jam']; ?></td>                        
                        <td>                    
                            <a href="edit_jadwal.php?id_jadwal=<?php echo $data['id_jadwal']; ?>&aksi_jadwal=edit_jadwal" class="btn btn-info">Edit</a>
                            <a href="proses_jb.php?id_jadwal=<?php echo $data['id_jadwal']; ?>&aksi_jadwal=delete_jadwal" class="btn btn-danger" onclick="return confirm('Yakin untuk menghapus data?')">Hapus</a>                        
                        </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>