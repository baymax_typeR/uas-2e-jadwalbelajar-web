<?php
class database{
    public $host = "localhost",
    $uname = "root",
    $pass = "",
    $db = "jadwal_kbm",
    $connect;    
    
    function __construct(){
        $this->connect = new mysqli($this->host, $this->uname, $this->pass, $this->db);
        if($this->connect->connect_errno){
            echo "Database tidak ada!";
            exit;
        }
    }
    
    //tutor
    function show_tutor(){
        $data = "select * from pengajar";
        $hasil = $this->connect->query($data);
        while ($d = mysqli_fetch_array($hasil)){
            $result[] = $d;
        }
        return $result;
    }
    
    function input_tutor($id_pengajar, $nama_pengajar, $alamat_pengajar, $telepon){
        $simpan = "insert into pengajar values('$id_pengajar','$nama_pengajar','$alamat_pengajar','$telepon')";
        $hasil = $this->connect->query($simpan);
    }
    
    function delete_tutor($id_pengajar){
        $simpan = "delete from pengajar where id_pengajar='$id_pengajar'";
        $hasil = $this->connect->query($simpan);
    }
    
    function edit_tutor($id_pengajar){
        $data = "select * from pengajar where id_pengajar='$id_pengajar'";
        $hasil = $this->connect->query($data);
        while ($d = mysqli_fetch_array($hasil)){
            $result[] = $d;            
        }
        return $result;
    }
    
    function update_tutor($id_pengajar, $nama_pengajar, $alamat_pengajar, $telepon){
        $simpan = "update pengajar set nama_pengajar='$nama_pengajar', alamat_pengajar='$alamat_pengajar', telepon='$telepon' where id_pengajar='$id_pengajar'";
        $hasil = $this->connect->query($simpan);
    }      
    
    //mapel
    function show_mapel(){
        $data = "select * from mata_pelajaran";
        $hasil = $this->connect->query($data);
        while ($d = mysqli_fetch_array($hasil)){
            $result[] = $d;
        }
        return $result;
    }
    
    function input_mapel($id_mapel, $nama_mapel, $kelas){
        $simpan = "insert into mata_pelajaran values('$id_mapel','$nama_mapel','$kelas')";
        $hasil = $this->connect->query($simpan);
    }
    
    function delete_mapel($id_mapel){
        $simpan = "delete from mata_pelajaran where id_mapel='$id_mapel'";
        $hasil = $this->connect->query($simpan);
    }
    
    function edit_mapel($id_mapel){
        $data = "select * from mata_pelajaran where id_mapel='$id_mapel'";
        $hasil = $this->connect->query($data);
        while ($d = mysqli_fetch_array($hasil)){
            $result[] = $d;            
        }
        return $result;
    }
    
    function update_mapel($id_mapel, $nama_mapel, $kelas){
        $simpan = "update mata_pelajaran set nama_mapel='$nama_mapel', kelas='$kelas' where id_mapel='$id_mapel'";
        $hasil = $this->connect->query($simpan);
    }

    //jadwal
    function show_jadwal(){
        $data = "select * from jadwal";
        $hasil = $this->connect->query($data);
        while ($d = mysqli_fetch_array($hasil)){
            $result[] = $d;
        }
        return $result;
    }
    
    function show_jadwal1(){
        $data = "select * from jadwal where tanggal=now()";
        $hasil = $this->connect->query($data);
        while ($d = mysqli_fetch_array($hasil)){
            $result[] = $d;
        }
        return $result;
    }

    function input_jadwal($id_jadwal, $tanggal, $nama_pengajar, $nama_mapel, $kelas, $jam){
        $simpan = "insert into jadwal values('$id_jadwal', '$tanggal', '$nama_pengajar', '$nama_mapel', '$kelas', '$jam')";
        $hasil = $this->connect->query($simpan);
    }
    
    function delete_jadwal($id_jadwal){
        $simpan = "delete from jadwal where id_jadwal='$id_jadwal'";
        $hasil = $this->connect->query($simpan);
    }
    
    function edit_jadwal($id_jadwal){
        $data = "select * from jadwal where id_jadwal='$id_jadwal'";
        $hasil = $this->connect->query($data);
        while ($d = mysqli_fetch_array($hasil)){
            $result[] = $d;            
        }
        return $result;
    }
    
    function update_jadwal($id_jadwal, $tanggal, $nama_pengajar, $nama_mapel, $kelas, $jam){
        $simpan = "update jadwal set tanggal='$tanggal', nama_pengajar='$nama_pengajar', nama_mapel='$nama_mapel', kelas='$kelas', jam='$jam' where id_jadwal='$id_jadwal'";
        $hasil = $this->connect->query($simpan);
    }
}
?>