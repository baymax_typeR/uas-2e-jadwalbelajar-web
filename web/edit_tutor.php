<?php
    include 'koneksi_jb.php';
    include 'koneksi.php';
    $db = new database();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Tutor | Jadwal Belajar</title>
  </head>
  <body>
    <?php
        include 'navbar.php';
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="mt-3">Selamat Datang di Website Jadwal Belajar</h2>
                <h3 class="mt-1">Edit Data Tutor</h3>                
                <br>                

                <?php foreach ($db->edit_tutor($_GET['id_pengajar']) as $data){ ?>
                <form action="proses_jb.php?aksi_tutor=update_tutor" method="post">  
                <div class="form-group row">
                      <label for="id_pengajar" class="col-sm-2 col-form-label">ID Pengajar</label>
                      <div class="col-sm-4">
                          <input type="text" readonly class="form-control" value="<?php echo $data['id_pengajar'] ?>"
                          name="id_pengajar">
                      </div>                    
                  </div>                                                   
                  <div class="form-group row">
                      <label for="nama_pengajar" class="col-sm-2 col-form-label">Nama Pengajar</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" value="<?php echo $data['nama_pengajar'] ?>"
                          name="nama_pengajar">
                      </div>                    
                  </div>          
                  <div class="form-group row">
                      <label for="alamat_pengajar" class="col-sm-2 col-form-label">Alamat</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" value="<?php echo $data['alamat_pengajar'] ?>"
                          name="alamat_pengajar">
                      </div>                    
                  </div>          
                  <div class="form-group row">
                      <label for="telepon" class="col-sm-2 col-form-label">Telepon</label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" value="<?php echo $data['telepon'] ?>"
                          name="telepon">
                      </div>                    
                  </div>                                                 
                  <div class="form-group row">
                      <label for="" class="col-sm-2 col-form-label"></label>
                      <div class="col-sm-1">
                        <button type="submit" class="btn btn-success" onclick="return confirm('Apakah data yang dimasukkan sudah benar?')">Submit</button>                
                      </div>     
                      <div class="col-sm-16">
                        <a href="tutor.php" class="btn btn-secondary">Kembali</a>               
                      </div>     
                  </div>
                <?php } ?>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>