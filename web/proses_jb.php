<?php
    include 'koneksi_jb.php';
    $db = new database();

//tutor
$aksi1 = $_GET['aksi_tutor'];
if ($aksi1 == "input_tutor"){
    $db->input_tutor($_POST['id_pengajar'], $_POST['nama_pengajar'], $_POST['alamat_pengajar'], $_POST['telepon']);
    header("location:tutor.php");
}
else if ($aksi1 == "delete_tutor"){
    $db->delete_tutor($_GET['id_pengajar']);
    header("location:tutor.php");
}
else if ($aksi1 == "edit_tutor"){
    $db->edit_tutor($_POST['id_pengajar']);
    header("location:tutor.php");
}
else if ($aksi1 == "update_tutor"){
    $db->update_tutor($_POST['id_pengajar'], $_POST['nama_pengajar'], $_POST['alamat_pengajar'], $_POST['telepon']);
    header("location:tutor.php");
}

//mapel
$aksi2 = $_GET['aksi_mapel'];
if ($aksi2 == "input_mapel"){
    $db->input_mapel($_POST['id_mapel'], $_POST['nama_mapel'], $_POST['kelas']);
    header("location:mapel.php");
}
else if ($aksi2 == "delete_mapel"){
    $db->delete_mapel($_GET['id_mapel']);
    header("location:mapel.php");
}
else if ($aksi2 == "edit_mapel"){
    $db->edit_mapel($_POST['id_mapel']);
    header("location:mapel.php");
}
else if ($aksi2 == "update_mapel"){
    $db->update_mapel($_POST['id_mapel'], $_POST['nama_mapel'], $_POST['kelas']);
    header("location:mapel.php");
}

//jadwal
$aksi3 = $_GET['aksi_jadwal'];
if ($aksi3 == "input_jadwal"){
    $db->input_jadwal($_POST['id_jadwal'], $_POST['tanggal'], $_POST['nama_pengajar'], $_POST['nama_mapel'],
    $_POST['kelas'], $_POST['jam']);
    header("location:jadwal.php");
}
else if ($aksi3 == "delete_jadwal"){
    $db->delete_jadwal($_GET['id_jadwal']);
    header("location:jadwal.php");
}
else if ($aksi3 == "edit_jadwal"){
    $db->edit_jadwal($_POST['id_jadwal']);
    header("location:jadwal.php");
}
else if ($aksi3 == "update_jadwal"){
    $db->update_jadwal($_POST['id_jadwal'], $_POST['tanggal'], $_POST['nama_pengajar'], $_POST['nama_mapel'],
    $_POST['kelas'], $_POST['jam']);
    header("location:jadwal.php");
}

?>