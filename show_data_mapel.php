<?php
    $DB_NAME = "jadwalbelajar";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
      $sql = "select nama_mapel from mata_pelajaran";
      $result = mysqli_query($conn, $sql);
      $row = mysqli_num_rows($result);
      if ($row > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Contact-type: application/json; charset=UTF-8");

        $data_mapel = array();
        while ($mapel = mysqli_fetch_assoc($result)){
          array_push($data_mapel, $mapel);
        }
        echo json_encode($data_mapel);
      }
    }
 ?>
